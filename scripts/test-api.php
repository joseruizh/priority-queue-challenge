<?php
include 'functions.php';
$requestsNum = $argv[1];
$timeBetweenRequestsMs = $argv[2] ?? 0;

$userToken = getUserToken('user1@priority.queue.challenge', 'password1');
$numJobsSent = 0;
$numGetSent = 0;
$numFails = 0;
$idLastJobSent = 0;
$start = microtime(TRUE);
for ($i = 0; $i < $requestsNum; $i++) {
    usleep($timeBetweenRequestsMs);
    $option = rand(0, 9);
    // 30 %
    if ($option < 3) {
        if (!getJob($userToken, rand(1, $numJobsSent))) {
            $numFails++;
        }
        $numGetSent++;
    } else { // 70%
        if (!createJob($userToken, 'ls -l')) {
            $numFails++;
        }
        $numJobsSent++;
    }
}
$end = microtime(TRUE);
$totalTime = ($end - $start);
$avgProcessTime = $totalTime / $requestsNum;

echo "Total Requests Time: $totalTime seconds\n";
echo "Avg Requests Process Time: $avgProcessTime seconds\n";
echo "Number of jobs sent: $numJobsSent\n";
echo "Number of queries: $numGetSent\n";
echo "Number of fails: $numFails\n";



