<?php
const ENDPOINT_URL = 'http://localhost';

/**
 * @param string $email
 * @param string $password
 * @return mixed|string|null
 */
function getUserToken(string $email, string $password)
{
    $response = httpRequest('/api/auth/login', 'POST', compact('email', 'password'));
    return $response['access_token'] ?? null;
}

/**
 * @param string $token
 * @param string $command
 * @return mixed|string
 */
function createJob(string $token, string $command)
{
    $authorization = "Authorization: Bearer $token";
    $response = httpRequest('/api/priority-queues', 'POST', compact('command'), [$authorization]);
//    print_r($response);
    return $response['id'] ?? null;
}

/**
 * @param string $token
 * @param $id
 */
function getJob(string $token, $id)
{
    $authorization = "Authorization: Bearer $token";
    $response = httpRequest("/api/priority-queues/$id", 'GET', [], [$authorization]);
//    print_r($response);
    return $response['id'] ?? null;
}

/**
 * @param string $uri
 * @param string $method
 * @param array $data
 * @param array $headers
 * @return bool|string
 */
function httpRequest(string $uri, string $method = 'GET', array $data = [], array $headers = [])
{
    $curl = curl_init();

    $curlOptions = [
        CURLOPT_URL => ENDPOINT_URL . $uri,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => strtoupper($method) === 'POST' ? 'POST' : 'GET',
        CURLOPT_HTTPHEADER => array_merge(["Accept: application/json"], $headers)
    ];
    if (strtoupper($method) === 'POST') {
        $curlOptions[CURLOPT_POSTFIELDS] = $data;
    }

    curl_setopt_array($curl, $curlOptions);

    $response = curl_exec($curl);

    curl_close($curl);
    return json_decode($response, true);
}
