# Priority Queue Challege
Giant Monkey Robot

Priority queue challenge is a project created as a test for the PHP Developer position in the company Giant Monkey Robot.

The project consits in implement a PHP Software that use a priority queue to process jobs, each job executes a command, the jobs are created through an API REST and also users can consult the Job's status and the Job's submitter using the API.

[More details about the challenge](https://bitbucket.org/joseruizh/priority-queue-challenge/raw/b5b90ed2945436ac6c8eb0518be2a4825c8ff012/PHP%20CHALLENGE%20V1.pdf)
### Tech

  - PHP 7.x
  - Laravel 7
  - JWT
  - MySQL
  - Redis
  - RabbitMQ
  - Docker
  - Docker compose
  - Supervisor
  - Git

### Requirements

  - Git client
  - Docker [ >= 17.12 ]
  - Docker compose
  
### Installation

Clone the following git repository (don't forget the `--recursive` option)
```sh
$ git clone --recursive https://bitbucket.org/joseruizh/priority-queue-challenge.git
```

Enter to the project directory and install the dependencies
```sh
$ cd priority-queue-challenge/
$ composer install
```

Copy the environment variables into `.env` using the `.env.example` file
```sh
$ cp .env.example .env
```

Now, enter to the laradock directory (Docker Project), and copy the `env-example` file into a file called `.env`
```sh
$ cp env-example .env
```

Run the following command to initialize all the containers ans services needed for th project
```sh
$ docker-compose up -d nginx mysql redis rabbitmq php-worker
```
if you don't have docker permissions properly configured, you can use `sudo`
```sh
$ sudo docker-compose up -d nginx mysql redis rabbitmq php-worker
```
this can take several minutes, depending of the Internet connection and the hardware of the computer

Next, connect to the workspace container and execute the `migrate` command and `db:reset`, this last command will populate the users table (1000 users) to login and make requests
```sh
$ docker-compose exec workspace bash
-# migrate
-# db:reset
```
### email/password format
```sh
user1@priority.queue.challenge / password1
user2@priority.queue.challenge / password2
...
user1000@priority.queue.challenge / password1000
```

Add the following configuration to youur hosts file
```sh
127.0.0.1 priority.queue.challenge
```

Thus, you can access the API endpoints and rabbit web page using this hostname

### php-worker
By default the `php-worker` executes 8 workers to process the rabbit queue, it is configurable in `laradock/php-worker/supervisord.d/laravel-worker.conf`, you need to restart the container for it to take effect.
### API documentation
https://documenter.getpostman.com/view/11945575/T17Q6QTW?version=latest

### RabbitMQ page url
http://priority.queue.challenge:15672/

### API endpoint url
http://priority.queue.challenge

### useful commands
```sh
$ docker-compose ps
$ docker-compose stop
$ docker-compose exec workspace bash
$ docker-compose exec php-worker sh
$ docker-compose up -d --build php-worker
$ git submodule foreach git pull origin master
```

### tests
Inside the scripts directory in the root of the project there are 2 files written in PHP copy to your machine and execute the file `test-api.php` the first argument is the number of request you want to make to the service.
```sh
$ php test-api.php 1000
```
`Notice: Currently the API rate limiting is 1000 requests per minute if you need to increment this value you can do it in the path app/Http/Kernel.php`

```sh
'api' => [
    'throttle:1000,1',
    \Illuminate\Routing\Middleware\SubstituteBindings::class,
]
```

### Todos

 - Write Tests

License
----
MIT
