<?php

namespace App\Providers;

use App\ExecuteCommandJob;
use Illuminate\Queue\Events\JobExceptionOccurred;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Queue::before(function (JobProcessing $event) {
            $this->updateJobStatus($event->job->getJobId(), ExecuteCommandJob::RUNNING);
        });

        Queue::after(function (JobProcessed $event) {
            $this->updateJobStatus($event->job->getJobId(), ExecuteCommandJob::DONE);
        });

        Queue::failing(function (JobFailed $event) {
            $this->updateJobStatus($event->job->getJobId(), ExecuteCommandJob::FAILED);
        });
    }

    /**
     * @param $jobId
     * @param $status
     */
    public function updateJobStatus($jobId, $status): void
    {
        $executeCommandJob = ExecuteCommandJob::where('job_id', $jobId)->first();
        $executeCommandJob->status = $status;
        $executeCommandJob->save();
        Cache::forget(env('JOB_CACHE_PREFIX') . "." . $executeCommandJob->id);

    }
}
