<?php

namespace App\Http\Controllers;

use App\ExecuteCommandJob;
use App\Jobs\executeCommand;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpFoundation\JsonResponse;
use Validator;

class PriorityQueueController extends Controller
{
    /**
     * PriorityQueueController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
         * Store and Dispatch a new CommandJob
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'command' => 'required|string|max:100',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $executeCommandJob = $this->createAndDispatchJob($validator->validated()['command']);

        Cache::put(
            env('JOB_CACHE_PREFIX') . "." . $executeCommandJob->getAttribute('id'),
            json_encode($executeCommandJob->getApiResponseArray()),
            env('JOB_CACHE_LIFETIME')
        );

        return new JsonResponse($executeCommandJob->getApiResponseArray());
    }

    /**
     * Get details of a Job by Id
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $data = Cache::remember(
            env('JOB_CACHE_PREFIX') . ".$id",
            env('JOB_CACHE_LIFETIME'),
            function () use ($id) {
                return json_encode(ExecuteCommandJob::findOrFail($id)->getApiResponseArray());
            });

        return new JsonResponse($data, 200, [], true);
    }

    /**
     * Create and Dispatch the Job
     *
     * @param string $command
     * @return mixed
     */
    private function createAndDispatchJob(string $command)
    {
        $executeCommandJob = ExecuteCommandJob::create([
            'submitter_id' => Auth::user()->getAttribute('id'),
            'command' => $command,
            'status' => ExecuteCommandJob::QUEUED
        ]);

        $executeCommand = new executeCommand($executeCommandJob);

        ExecuteCommandJob::where('id', $executeCommandJob->getAttribute('id'))
            ->update(['job_id' => app(Dispatcher::class)
                ->dispatch($executeCommand->delay(5))]);

        return $executeCommandJob;
    }
}
