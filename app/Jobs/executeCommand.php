<?php

namespace App\Jobs;

use App\ExecuteCommandJob;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class executeCommand implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * @var ExecuteCommandJob
     */
    private $executeCommandJob;

    /**
     * executeCommand constructor.
     * @param ExecuteCommandJob $executeCommandJob
     */
    public function __construct(ExecuteCommandJob $executeCommandJob)
    {
        $this->executeCommandJob = $executeCommandJob->withoutRelations();
    }

    /**
     * @return int
     * @throws ProcessFailedException
     * @throws \Exception
     */
    public function handle()
    {
        try {
            $command = $this->executeCommandJob->getAttribute('command');
            $this->processCommand($command);

        } catch (\Exception $e) {
            $this->executeCommandJob->setAttribute('status', ExecuteCommandJob::DELAYED)->save();
            Cache::forget(env('JOB_CACHE_PREFIX') . "." . $this->executeCommandJob->getAttribute('id'));
            throw $e;
        }

        return 0;
    }

    /**
     * @param $command
     */
    public function processCommand($command): void
    {
        Log::info("Executing command: [$command]");

        $process = new Process(explode(' ', $command));
        $process->run();

        if (!$process->isSuccessful()) {
            Log::error("[$command]: " . $process->getOutput());
            throw new ProcessFailedException($process);
        }
        Log::info("[$command]: " . $process->getOutput());
    }


}
