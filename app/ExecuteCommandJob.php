<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExecuteCommandJob extends Model
{
    const QUEUED    = 'QUEUED';
    const RUNNING   = 'RUNNING';
    const DONE      = 'DONE';
    const FAILED    = 'FAILED';
    const DELAYED   = 'DELAYED';

    protected $fillable = [
        'submitter_id', 'command', 'status',
    ];

    public function submitter()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return array
     */
    public function getApiResponseArray()
    {
        return [
            'id' => $this->getAttribute('id'),
            'submitter' => $this->getAttribute('submitter')->getApiResponseArray(),
            'command' => $this->getAttribute('command'),
            'status' => $this->getAttribute('status')
        ];
    }
}
