<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    const NUMBER_OF_USERS = 1000;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= self::NUMBER_OF_USERS; $i++) {
            DB::table('users')->insert([
                'name' => "user$i",
                'email' => "user$i@priority.queue.challenge",
                'password' => Hash::make("password$i"),
            ]);
        }

    }
}
