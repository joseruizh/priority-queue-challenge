<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJobIdColumnToExecuteCommandJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('execute_command_jobs', function (Blueprint $table) {
            $table->string('job_id', 100)->after('submitter_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('execute_command_jobs', function (Blueprint $table) {
            $table->dropColumn(['job_id']);
        });
    }
}
