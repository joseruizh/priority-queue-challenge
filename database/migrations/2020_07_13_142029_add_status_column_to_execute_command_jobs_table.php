<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusColumnToExecuteCommandJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('execute_command_jobs', function (Blueprint $table) {
            $table->enum('status', ['QUEUED', 'RUNNING', 'DONE', 'FAILED', 'DELAYED'])->after('job_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('execute_command_jobs', function (Blueprint $table) {
            $table->dropColumn(['status']);
        });
    }
}
