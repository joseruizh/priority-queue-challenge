<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJobIdIndexToExecuteCommandJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('execute_command_jobs', function (Blueprint $table) {
            $table->index('job_id', 'job_id_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('execute_command_jobs', function (Blueprint $table) {
            $table->dropIndex('job_id_idx');
        });
    }
}
